using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotnet.Data;
using empresas_dotnet.Models;
using empresas_dotnet.Services;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.Repositories
{
    public class UserRepository : IUserService
    {
        private readonly TesteDataContext _context;
        public UserRepository(TesteDataContext context)
        {
            _context = context;
        }
        public void Create(UserModel model)
        {
            _context.Users.Add(model);            
        }

        public Task<List<UserModel>> GetAllActives(int offset)
        {

            return _context.Users
                .Include(x => x.Profile)
                .OrderBy(x => x.FullName)
                .OrderBy(x => x.Login)
                .Where(x => x.Active == true && 
                    !x.Profile.ProfileName.Equals("Administrator"))
                .Take(30)
                .Skip(offset)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<UserModel> GetByIdAsync(long id)
        {
            return _context.Users
                .Include(x => x.Profile)                
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public Task<UserModel> GetByLoginAsyncNoTracking(string login)
        {
            return _context.Users
                .Include(x => x.Profile)
                .AsNoTracking()
                .SingleOrDefaultAsync(x => 
                    x.Login.ToLower().Equals(
                            login.ToLower().Trim()));
        }
        public void Remove(long Id)
        {
            throw new System.NotImplementedException();
        }
        public void Update(UserModel model)
        {
            _context.Users.Update(model);
        }
    }
}