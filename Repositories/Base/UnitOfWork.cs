using empresas_dotnet.Data;
using empresas_dotnet.Services;

namespace empresas_dotnet.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TesteDataContext _context;
        public UnitOfWork(TesteDataContext context)
        {
            _context = context;
        }
        public void Commit()
        {
            _context.SaveChanges();
        }

        public void RollBack()
        {
            // nothing to do
        }
    }
}