using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotnet.Data;
using empresas_dotnet.Models;
using empresas_dotnet.Services;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.Repositories
{
    public class MovieRepository : IMovie
    {
        private readonly TesteDataContext _context;
        public MovieRepository(TesteDataContext context)
        {
            _context = context;
        }
        public void Create(MovieModel model)
        {
            _context.Movies.Add(model);
        }

        public Task<List<MovieModel>> GetAll(string director, string title, string genre, string[] actors, int offset)
        {
            return _context.Movies
                .Include(x => x.ActorMovies)
                    .ThenInclude(x => x.Actor)
                .Where(x => 
                    (string.IsNullOrEmpty(director) || x.Director.ToLower().Equals(director.ToLower())) &&
                    (string.IsNullOrEmpty(title) || x.Title.ToLower().Contains(title.ToLower())) &&
                    (string.IsNullOrEmpty(genre) || x.Genre.ToLower().Equals(genre.ToLower())) &&                    
                    // (actors.Count() == 0 || actors.All(actor => x.ActorMovies.Any(y => y.Actor.FullName.ToLower().StartsWith(actor.ToLower()) ))  ))
                    (actors.Count() == 0 || x.ActorMovies.Any(x => actors.Contains(x.Actor.FullName.ToLower()))))
                .OrderByDescending(x => x.RatingCount)
                .OrderBy(x => x.Title)
                .Take(30)
                .Skip(offset)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<MovieModel> GetById(long id)
        {
            return _context.Movies
                .Include(x => x.ActorMovies)
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync();
        }

        public Task<MovieModel> GetByTitleAsyncNoTracking(string title)
        {
            return _context.Movies
                .Include(x => x.ActorMovies)
                .Where(x => 
                    x.Title.ToLower()
                        .Equals(title.ToLower()))
                .SingleOrDefaultAsync();
        }

        public void Remove(long Id)
        {
            throw new System.NotImplementedException();
        }

        public void Update(MovieModel model)
        {
            _context.Movies.Update(model);
        }
    }
}