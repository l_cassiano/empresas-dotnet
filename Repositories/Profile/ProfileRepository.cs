using System.Linq;
using System.Threading.Tasks;
using empresas_dotnet.Data;
using empresas_dotnet.Models;
using empresas_dotnet.Services;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.Repositories
{
    public class ProfileRepository : IProfileService
    {
        private readonly TesteDataContext _context;
        public ProfileRepository(TesteDataContext context)
        {
            _context = context;
        }
        public Task<ProfileModel> GetByProfileName(string name)
        {
            return _context.Profiles
                .Where(x =>
                    x.ProfileName.ToLower()
                        .Equals(name.ToLower()))
                .SingleOrDefaultAsync();
        }
    }
}