using empresas_dotnet.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Maps
{
    public class ActorMap : IEntityTypeConfiguration<ActorModel>
    {
        public void Configure(EntityTypeBuilder<ActorModel> o)
        {
            o.ToTable("actors");

            o.HasKey(x => x.Id);

            o.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("bigint")
                .UseSerialColumn<long>()
                .ValueGeneratedOnAdd()
                .IsRequired();

            o.Property(x => x.FullName)
                .HasColumnName("full_name")
                .HasColumnType("varchar(255)")
                .IsRequired();

            o.Property(x => x.CreatedAt)
                .HasColumnName("created_at")
                .HasColumnType("timestamp")
                .IsRequired();

            o.Property(x => x.UpdatedAt)
                .HasColumnName("updated_at")
                .HasColumnType("timestamp")
                .IsRequired();            
        }
    }
}