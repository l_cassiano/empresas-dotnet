using empresas_dotnet.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Maps
{
    public class MovieActorMap : IEntityTypeConfiguration<ActorMoviesModel>
    {
        public void Configure(EntityTypeBuilder<ActorMoviesModel> o)
        {
            o.ToTable("actor_movies");

            o.HasKey(x => new { x.IdActor, x.IdMovie });

            o.Property(x => x.IdActor)
                .HasColumnName("id_actor")
                .HasColumnType("bigint")
                .IsRequired();

            o.Property(x => x.IdMovie)
                .HasColumnName("id_movie")
                .HasColumnType("bigint")
                .IsRequired();

            o.HasOne(x => x.Actor)
                .WithMany(x => x.ActorMovies)
                .HasForeignKey(x => x.IdActor)
                .HasConstraintName("fk_actors_actor_movies_id_actor")
                .OnDelete(DeleteBehavior.Restrict);

            o.HasOne(x => x.Movie)
                .WithMany(x => x.ActorMovies)
                .HasForeignKey(x => x.IdMovie)
                .HasConstraintName("fk_movies_actor_movies_id_movie")
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}