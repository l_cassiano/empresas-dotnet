using empresas_dotnet.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Maps
{
    public class MovieMap : IEntityTypeConfiguration<MovieModel>
    {
        public void Configure(EntityTypeBuilder<MovieModel> o)
        {
            o.ToTable("movies");

            o.HasKey(x => x.Id);

            o.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("bigint")
                .UseSerialColumn<long>()
                .ValueGeneratedOnAdd()
                .IsRequired();

            o.Property(x => x.Title)
                .HasColumnName("title")
                .HasColumnType("varchar(255)")
                .IsRequired();

            o.Property(x => x.Description)
                .HasColumnName("description")
                .HasColumnType("text")
                .IsRequired();

            o.Property(x => x.Director)
                .HasColumnName("director")
                .HasColumnType("varchar(255)")
                .IsRequired();

            o.Property(x => x.Genre)
                .HasColumnName("genre")
                .HasColumnType("varchar(255)")
                .IsRequired();

            o.Property(x => x.Rating)
                .HasColumnName("rating")
                .HasColumnType("integer")
                .IsRequired();

            o.Property(x => x.RatingCount)
                .HasColumnName("rating_count")
                .HasColumnType("integer")
                .IsRequired();
            
            o.Property(x => x.CreatedAt)
                .HasColumnName("created_at")
                .HasColumnType("timestamp")
                .IsRequired();

            o.Property(x => x.UpdatedAt)
                .HasColumnName("updated_at")
                .HasColumnType("timestamp")
                .IsRequired();
        }
    }
}