using empresas_dotnet.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet
{
    public class ProfileMap : IEntityTypeConfiguration<ProfileModel>
    {
        public void Configure(EntityTypeBuilder<ProfileModel> o)
        {
            o.ToTable("profiles");

            o.HasKey(x => x.Id);

            o.HasKey(x => x.Id);
            
            o.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("bigint")
                .UseSerialColumn<long>()
                .ValueGeneratedOnAdd();

            o.Property(x => x.ProfileName)
                .HasColumnName("profile_name")
                .HasColumnType("varchar(50)")
                .IsRequired();

            o.Property(x => x.CreatedAt)
                .HasColumnName("created_at")
                .HasColumnType("timestamp")
                .IsRequired();

            o.Property(x => x.UpdatedAt)
                .HasColumnName("updated_at")
                .HasColumnType("timestamp")
                .IsRequired();
        }
    }
}