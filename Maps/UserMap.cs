using empresas_dotnet.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Maps
{
    public class UserMap : IEntityTypeConfiguration<UserModel>
    {
        public void Configure(EntityTypeBuilder<UserModel> o)
        {
            o.ToTable("users");

            o.HasKey(x => x.Id);
            
            o.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("bigint")
                .UseSerialColumn<long>()
                .ValueGeneratedOnAdd();

            o.Property(x => x.FullName)
                .HasColumnName("full_name")
                .HasColumnType("varchar(255)")
                .IsRequired();

            o.Property(x => x.Login)   
                .HasColumnName("login")
                .HasColumnType("varchar(255)")
                .IsRequired();

            o.HasIndex(x => x.Login)
                .IsUnique();

            o.Property(x => x.Password)
                .HasColumnName("password")
                .HasColumnType("varchar(255)")
                .IsRequired();

            o.Property(x => x.Active)
                .HasColumnName("active")
                .HasColumnType("boolean")
                .IsRequired();

            o.Property(x => x.CreatedAt)
                .HasColumnName("created_at")
                .HasColumnType("timestamp")
                .IsRequired();

            o.Property(x => x.UpdatedAt)
                .HasColumnName("updated_at")
                .HasColumnType("timestamp")
                .IsRequired();

            o.Property(x => x.IdProfile)
                .HasColumnName("id_profile")
                .HasColumnType("bigint")
                .IsRequired();
            
            o.HasOne(x => x.Profile)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.IdProfile)
                .HasConstraintName("fk_users_profiles_id_profile")
                .OnDelete(DeleteBehavior.Restrict);            
        }
    }
}