using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class ActorViewModel
    {
        [JsonProperty(PropertyName = "fullName")]
        public string FullName { get; set; }
    }
}