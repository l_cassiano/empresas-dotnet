using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class MovieRateViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "rate")]
        public int Rate { get; set; }
    }
}