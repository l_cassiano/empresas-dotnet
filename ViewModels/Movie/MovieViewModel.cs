using System.Collections.Generic;
using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class MovieViewModel
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "director")]
        public string Director { get; set; }

        [JsonProperty(PropertyName = "genre")]
        public string Genre { get; set; }

        [JsonProperty(PropertyName = "rating")]
        public int Rating { get; set; }

        [JsonProperty(PropertyName = "average")]
        public double Average { get; set; }

        [JsonProperty(PropertyName = "actors")]
        public List<ActorViewModel> Actors { get; set; }
    }
}