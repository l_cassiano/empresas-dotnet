using System.Collections.Generic;
using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class ResponseSuccessViewModel
    {
        [JsonProperty(PropertyName = "result")]
        public object Results { get; set; }

        [JsonProperty(PropertyName = "status_message")]
        public string StatusMessage { get; set; }
    }
}