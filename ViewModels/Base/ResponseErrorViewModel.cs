using System.Collections.Generic;
using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class ResponseErrorViewModel
    {
        [JsonProperty(PropertyName = "errors")]
        public List<object> Errors { get; set; }

        [JsonProperty(PropertyName = "status_message")]
        public string StatusMessage { get; set; }
    }
}