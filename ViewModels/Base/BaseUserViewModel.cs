using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class BaseUserViewModel
    {        
        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }
}