using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class ProfileViewModel
    {
        [JsonProperty(PropertyName = "profile_name")]
        public string ProfileName { get; set; }
    }
}