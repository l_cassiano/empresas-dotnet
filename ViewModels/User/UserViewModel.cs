using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class UserViewModel : BaseUserViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "full_name")]
        public string FullName { get; set; }

        [JsonProperty(PropertyName = "profile")]
        public ProfileViewModel Profile { get; set; }
    }
}