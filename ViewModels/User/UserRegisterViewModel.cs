using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class UserRegisterViewModel : BaseUserViewModel
    {           
        [JsonProperty(PropertyName = "fullName")]        
        public string FullName { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
    }
}