using Newtonsoft.Json;

namespace empresas_dotnet.ViewModels
{
    public class UserLoginSuccessViewModel : UserViewModel
    {        
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }
    }
}