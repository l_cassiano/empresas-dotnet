using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using empresas_dotnet.Configurations;
using empresas_dotnet.Data;
using empresas_dotnet.Repositories;
using empresas_dotnet.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;

namespace empresas_dotnet
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var jwtConfiguration = new JwtConfiguration(Configuration);

            var dataBaseConfiguration = new DatabaseConfiguration(Configuration);

            services.AddDbContext<TesteDataContext>(x =>
                x.UseNpgsql(dataBaseConfiguration.ConnectionString));

            services.AddAuthorization(x => new CustomPolicies(x));
            
            services.AddTransient<IAuthorizationHandler, CustomRequirementHandler>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IUserService, UserRepository>();
            services.AddTransient<IProfileService, ProfileRepository>();
            services.AddTransient<IMovie, MovieRepository>();
            
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ClockSkew = TimeSpan.Zero,
                    ValidateAudience = true,
                    ValidateIssuer = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwtConfiguration.Issuer,
                    ValidAudience = jwtConfiguration.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(jwtConfiguration.SymmetricSecurityKey)
                };

                x.Events = new JwtBearerEvents()
                {
                    OnMessageReceived = context =>
                    {
                        if (context.Request.Headers.ContainsKey("Authorization"))
                        {
                            var accessToken = context.Request.Headers["Authorization"].ToString();
                            context.Token = accessToken.Replace("Bearer", "").Trim();
                        }

                        return Task.CompletedTask;
                    }                    
                };
            });

            services.AddHttpContextAccessor();

            services.AddAutoMapper(typeof(Startup));

            services.AddControllers(x =>
            {
                var defaultAuthBuilder = new AuthorizationPolicyBuilder(
                    JwtBearerDefaults.AuthenticationScheme);

                var defaultAuthPolicy = defaultAuthBuilder
                    .RequireAuthenticatedUser()
                    .Build();

                x.Filters.Add(new AuthorizeFilter(defaultAuthPolicy));
            })
            .AddNewtonsoftJson(x =>
            {
                x.SerializerSettings.Formatting = Formatting.None;
                x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                x.SerializerSettings.Culture = CultureInfo.CurrentCulture;
            });

            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "Teste IoaSys",
                        Version = "1.0.0",
                        Description = "Projeto para teste",
                        Contact = new OpenApiContact
                        {
                            Email = "lucas_cassiano@live.com"
                        }
                    });

                x.AddSecurityDefinition("Bearer",
                    new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Description = "",
                        Name = "Authorization",
                        Type = SecuritySchemeType.ApiKey
                    });

                x.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }
                });

                x.ResolveConflictingActions(x => x.First());

                string applicationDirectory =
                    PlatformServices.Default.Application.ApplicationBasePath;

                string applicationName =
                    PlatformServices.Default.Application.ApplicationName;

                string directoryDocument =
                    Path.Combine(applicationDirectory, $"{applicationName}.xml");

                if (File.Exists(directoryDocument))
                {
                    x.IncludeXmlComments(directoryDocument);
                }
            });        
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("v1/swagger.json", "Teste IoaSys");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
