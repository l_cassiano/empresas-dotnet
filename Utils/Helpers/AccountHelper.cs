using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using empresas_dotnet.Configurations;
using empresas_dotnet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace empresas_dotnet.Utils
{
    public static class AccountHelper
    {        
        public static string Encrypt(string valor)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] hashBytes;

            using (HashAlgorithm hash = SHA256.Create())
                hashBytes = hash.ComputeHash(encoding.GetBytes(valor));

            StringBuilder hashValue = new StringBuilder(hashBytes.Length * 2);

            foreach (byte b in hashBytes)
            {
                hashValue.AppendFormat(CultureInfo.InvariantCulture, "{0:X2}", b);
            }

            return hashValue.ToString();
        }

        public static string GenerateToken(IConfiguration configuration, UserModel user)
        {
            var jwtConfiguration = new JwtConfiguration(configuration);

            var Claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, user.Login),
                new Claim("login", user.Login),
                new Claim("name", user.FullName),
                new Claim("profile", user.Profile.ProfileName)
            };

            var Created = DataHelper.CurrentDateTime();
            var Expires = Created.AddHours(1);

            var Key = new SymmetricSecurityKey(jwtConfiguration.SymmetricSecurityKey);

            var SigningCredentials = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256);

            var Token = new JwtSecurityToken(jwtConfiguration.Issuer, jwtConfiguration.Audience, Claims,
                        notBefore: Created, expires: Expires, SigningCredentials);

            var accessToken = new JwtSecurityTokenHandler().WriteToken(Token);

            return accessToken;
        }
        
        private static bool GetClaim(HttpContext httpContext, string value) =>
            httpContext.User.HasClaim("profile", value);
        public static bool IsPermit(HttpContext httpContext, string role) =>
            GetClaim(httpContext, role);
    }
}