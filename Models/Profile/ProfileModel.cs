using System;
using System.Collections.Generic;

namespace empresas_dotnet.Models
{
    public class ProfileModel : BaseModel
    {
        public string ProfileName { get; set; }
        public virtual List<UserModel> Users { get; set; }
    }
}