using System.Collections.Generic;

namespace empresas_dotnet.Models
{
    public class ActorModel : BaseModel
    {
        public string FullName { get; set; }
        public virtual List<ActorMoviesModel> ActorMovies { get; set; }
    }
}