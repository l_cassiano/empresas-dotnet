namespace empresas_dotnet.Models
{
    public class ActorMoviesModel
    {
        public long IdMovie { get; set; }
        public virtual MovieModel Movie { get; set; }
        public long IdActor { get; set; }
        public virtual ActorModel Actor { get; set; }        
    }
}