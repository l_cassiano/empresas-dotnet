using System.Collections.Generic;

namespace empresas_dotnet.Models
{
    public class MovieModel : BaseModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }
        public string Genre { get; set; }
        public int Rating { get; set; }
        public int RatingCount { get; set; }
        public virtual List<ActorMoviesModel> ActorMovies { get; set; }
    }
}