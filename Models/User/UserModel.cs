using System;

namespace empresas_dotnet.Models
{
    public class UserModel : BaseModel
    {        
        public string FullName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public long IdProfile { get; set; }
        public ProfileModel Profile { get; set; }        
    }
}