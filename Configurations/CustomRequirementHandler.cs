using System.Threading.Tasks;
using empresas_dotnet.Services;
using empresas_dotnet.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace empresas_dotnet.Configurations
{
    public class CustomRequirementHandler : AuthorizationHandler<CustomRequirement>
    {        
        private readonly HttpContext _httpContext;
        private readonly IUserService _userMananger;        
        public CustomRequirementHandler(
            IHttpContextAccessor httpContextAccessor,
            IUserService userMananger
        )
        {
            _httpContext = httpContextAccessor.HttpContext;
            _userMananger = userMananger;
        }

        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            CustomRequirement requirement)
        {            
            if (AccountHelper.IsPermit(_httpContext, 
                requirement.RequiredPermission))
                context.Succeed(requirement);

            return Task.CompletedTask;
        }    
    }
}