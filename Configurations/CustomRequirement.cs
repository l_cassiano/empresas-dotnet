using Microsoft.AspNetCore.Authorization;

namespace empresas_dotnet.Configurations
{
    public class CustomRequirement : IAuthorizationRequirement
    {
        public string RequiredPermission { get; }
        public CustomRequirement(string requiredPermission)
        {
            this.RequiredPermission = requiredPermission;
        }
    }
}