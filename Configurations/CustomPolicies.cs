using Microsoft.AspNetCore.Authorization;

namespace empresas_dotnet.Configurations
{
    public class CustomPolicies : AuthorizationOptions
    {
        public CustomPolicies(AuthorizationOptions x)
        {
            x.AddPolicy("Administrator", p =>
                p.Requirements.Add(new CustomRequirement("Administrator")));

            x.AddPolicy("UserDefault", p =>
                p.Requirements.Add(new CustomRequirement("UserDefault")));
        }
    }
}