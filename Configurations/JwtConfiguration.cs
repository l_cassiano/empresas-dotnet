using System.Text;
using Microsoft.Extensions.Configuration;

namespace empresas_dotnet.Configurations
{
    public class JwtConfiguration
    {
        public string Key { get; set; }
        public byte[] SymmetricSecurityKey { get; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public JwtConfiguration(IConfiguration configuration)
        {
            configuration.GetSection(this.GetType().Name).Bind(this);

            this.SymmetricSecurityKey = Encoding.UTF8.GetBytes(Key);
        }
    }
}