using Microsoft.Extensions.Configuration;

namespace empresas_dotnet.Configurations
{
    public class DatabaseConfiguration
    {
        public string ConnectionString { get; set; }

        public DatabaseConfiguration(IConfiguration configuration)
        {
            configuration.GetSection(this.GetType().Name).Bind(this);
        }
    }
}