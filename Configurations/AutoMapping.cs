using AutoMapper;
using empresas_dotnet.Models;
using empresas_dotnet.ViewModels;

namespace empresas_dotnet.Configurations
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {                        
            CreateMap<UserModel, UserRegisterViewModel>()            
            .ForMember(x => x.FullName, m => m.MapFrom(x => x.FullName))
            .ForMember(x => x.Login, m => m.MapFrom(x => x.Login))
            .ForMember(x => x.Password, m => m.MapFrom(x => x.Password))
            .ReverseMap();

            CreateMap<UserModel, UserLoginViewModel>()            
            .ForMember(x => x.Login, m => m.MapFrom(x => x.Login))
            .ForMember(x => x.Password, m => m.MapFrom(x => x.Password))
            .ReverseMap();

            CreateMap<UserModel, UserLoginSuccessViewModel>()
            .ForMember(x => x.FullName, m => m.MapFrom(x => x.FullName))
            .ForMember(x => x.Login, m => m.MapFrom(x => x.Login))
            .ForMember(x => x.Password, m => m.MapFrom(x => x.Password))            
            .ForPath(x => x.Profile, m => m.MapFrom(x => x.Profile))
            .ReverseMap();

            CreateMap<ProfileModel, ProfileViewModel>()
            .ForMember(x => x.ProfileName, m => m.MapFrom(x => x.ProfileName))
            .ReverseMap();
            
            CreateMap<MovieModel, MovieViewModel>()
            .ForMember(x => x.Average, m => m.MapFrom(x => (x.Rating / x.RatingCount)))
            .ForPath(x => x.Actors, m => m.MapFrom(x => x.ActorMovies))
            .ReverseMap();

            CreateMap<ActorModel, ActorViewModel>()            
            .ReverseMap();

            CreateMap<ActorViewModel, ActorMoviesModel>()
            .ForPath(x => x.Actor.FullName, m => m.MapFrom(x => x.FullName))
            .ReverseMap();
        }
    }
}