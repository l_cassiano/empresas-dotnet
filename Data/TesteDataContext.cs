using empresas_dotnet.Maps;
using empresas_dotnet.Models;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.Data
{
    public class TesteDataContext : DbContext
    {
        public DbSet<UserModel> Users { get; set; }
        public DbSet<ProfileModel> Profiles { get; set; }
        public DbSet<MovieModel> Movies { get; set; }
        public DbSet<ActorModel> Actors { get; set; }
        public DbSet<ActorMoviesModel> ActorsMovies { get; set; }
        public TesteDataContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new ProfileMap());
            modelBuilder.ApplyConfiguration(new MovieMap());
            modelBuilder.ApplyConfiguration(new ActorMap());
            modelBuilder.ApplyConfiguration(new MovieActorMap());

            modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }
    }
}