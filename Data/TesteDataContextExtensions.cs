using System.Collections.Generic;
using empresas_dotnet.Models;
using empresas_dotnet.Utils;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.Data
{
    public static class TesteDataContextExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            // create profiles
            var profiles = new List<ProfileModel>
            {
                new ProfileModel { Id = 1, ProfileName = "Administrator", 
                    CreatedAt = DataHelper.CurrentDateTime(), 
                        UpdatedAt = DataHelper.CurrentDateTime() },
                new ProfileModel { Id = 2, ProfileName = "UserDefault", 
                    CreatedAt = DataHelper.CurrentDateTime(),
                         UpdatedAt = DataHelper.CurrentDateTime() }
            };

            modelBuilder.Entity<ProfileModel>().HasData(profiles);
        }
    }
}