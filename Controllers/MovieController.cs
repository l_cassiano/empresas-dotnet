using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using empresas_dotnet.Models;
using empresas_dotnet.Services;
using empresas_dotnet.Utils;
using empresas_dotnet.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace empresas_dotnet.Controllers
{
    [Route("api/v1/[controller]"), ApiController]
    public class MovieController : ControllerBase
    {        
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly IMovie _movieMananger;

        public MovieController(            
            [FromServices] IUnitOfWork uow,
            [FromServices] IMapper mapper,
            [FromServices] IMovie movieMananger
        )
        {            
            _uow = uow;
            _mapper = mapper;
            _movieMananger = movieMananger;
        }

        [HttpPost, Route("Register"), Authorize(Policy = "administrator")]
        [Produces("application/json")]
        public async Task<IActionResult> Register(
            [FromBody] MovieViewModel movieViewModel
        )
        {
            try
            {
                var movieModel = _mapper
                    .Map<MovieModel>(movieViewModel);

                // invalid param
                if (string.IsNullOrEmpty(movieModel.Title) || 
                    string.IsNullOrEmpty(movieModel.Director) ||
                    string.IsNullOrEmpty(movieModel.Description) ||
                    string.IsNullOrEmpty(movieModel.Genre) &&
                    (movieModel.ActorMovies == null || 
                    movieModel.ActorMovies.Count == 0))
                    return BadRequest();

                var movieDb = await _movieMananger
                    .GetByTitleAsyncNoTracking(movieModel.Title.Trim());

                // movie found
                if (movieDb != null)
                    return Conflict();

                movieModel.Rating = 0;
                movieModel.RatingCount = 0;
                movieModel.CreatedAt = DataHelper.CurrentDateTime();
                movieModel.UpdatedAt = movieModel.CreatedAt;

                _movieMananger.Create(movieModel);

                _uow.Commit();

                var movieResponse = _mapper
                    .Map<MovieViewModel>(movieModel);

                var response = new ResponseSuccessViewModel
                {
                    Results = movieResponse,
                    StatusMessage = MessageHelper.SuccessResponse
                };

                return Created(new Uri(
                        Url.ActionLink("Register", "Movie")), response);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost, Route("Rate"), Authorize(Policy = "UserDefault")]
        [Produces("application/json")]
        public async Task<IActionResult> Rate(
            [FromBody] MovieRateViewModel movieRateViewModel
        )
        {
            try
            {
                // invalid param
                if (movieRateViewModel.Id <= 0 && 
                    (movieRateViewModel.Rate < 0 || 
                        movieRateViewModel.Rate > 4))
                    return BadRequest();

                var movieDb = await _movieMananger
                    .GetById(movieRateViewModel.Id);

                // not found
                if (movieDb == null)
                    return NotFound();

                movieDb.Rating += movieRateViewModel.Rate;
                movieDb.RatingCount += 1;
                movieDb.UpdatedAt = DataHelper.CurrentDateTime();
                
                _movieMananger.Update(movieDb);

                _uow.Commit();

                var response = new ResponseSuccessViewModel
                {
                    Results = null,
                    StatusMessage = MessageHelper.SuccessResponse
                };

                return Ok(response);
            }
            catch (Exception)
            {                
                return BadRequest();
            }
        }

        [HttpGet, Route(""), AllowAnonymous]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll(
            [FromQuery] string director,
            [FromQuery] string title,
            [FromQuery] string genre,
            [FromQuery] string[] actors,
            [FromQuery] int page = 1
        )
        {
            try
            {
                if (page < 1)
                    page = 1;

                var offset = (page == 1? 0: (page * 30));

                var moviesDb = await _movieMananger
                    .GetAll(director, title, genre, actors, offset);

                var moviesResponse = _mapper
                    .Map<List<MovieViewModel>>(moviesDb);

                var response = new ResponseSuccessViewModel
                {
                    Results = moviesResponse,
                    StatusMessage = MessageHelper.SuccessResponse
                };

                return Ok(response);
            }
            catch (Exception)
            {                
                return BadRequest();
            }
        }

    }
}