using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using empresas_dotnet.Models;
using empresas_dotnet.Services;
using empresas_dotnet.Utils;
using empresas_dotnet.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace empresas_dotnet.Controllers
{    
    [Route("api/v1/[controller]"), ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IConfiguration _configuration; 
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly IUserService _userMananger;
        private readonly IProfileService _profileMananger;
        public AccountController(
            [FromServices] IConfiguration configuration,
            [FromServices] IUnitOfWork uow,
            [FromServices] IMapper mapper,
            [FromServices] IUserService userMananger,
            [FromServices] IProfileService profileMananger
        )
        {
            _configuration = configuration;
            _uow = uow;
            _mapper = mapper;
            _userMananger = userMananger;
            _profileMananger = profileMananger;
        }

        /// <summary>
        /// Create a new user
        /// </summary>
        /// <remarks>        
        /// Type = administrator or userDefault
        /// </remarks>
        /// <param name="userViewModel"></param>
        /// <returns></returns>

        [HttpPost, Route("Register"), AllowAnonymous]
        [Produces("application/json")]
        public async Task<IActionResult> Register(
            [FromBody] UserRegisterViewModel userViewModel
        )
        {
            try
            {
                var userModel = _mapper
                    .Map<UserModel>(userViewModel);

                // invalid param
                if (string.IsNullOrEmpty(userModel.Login) || 
                    string.IsNullOrEmpty(userModel.Password) || 
                    string.IsNullOrEmpty(userViewModel.Type))
                    return BadRequest();

                var userDb = await _userMananger
                    .GetByLoginAsyncNoTracking(userModel.Login);

                // user found
                if (userDb != null)
                    return Conflict();

                // encrypt password
                userModel.Password = AccountHelper
                    .Encrypt(userModel.Password);
                
                userModel.Profile = await _profileMananger
                    .GetByProfileName(userViewModel.Type);

                // profile not found
                if (userModel.Profile == null)
                    return NotFound();

                // create new user
                userModel.Active = true;
                userModel.CreatedAt = DataHelper.CurrentDateTime();
                userModel.UpdatedAt = userModel.CreatedAt;

                _userMananger.Create(userModel);

                _uow.Commit();

                // remove password for response
                userViewModel.Password = null;

                var response = new ResponseSuccessViewModel
                {
                    Results = userViewModel,
                    StatusMessage = MessageHelper.SuccessResponse
                };

                return Created(new Uri(
                        Url.ActionLink("Register", "Account")), response);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        
        /// <summary>
        /// Login to user
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>

        [HttpPost, Route("Login"), AllowAnonymous]
        [Produces("application/json")]
        public async Task<IActionResult> Login(
            [FromBody] UserLoginViewModel userViewModel
        )
        {
            try
            {
                var userModel = _mapper
                    .Map<UserModel>(userViewModel);

                // invalid param
                if (string.IsNullOrEmpty(userModel.Login) || 
                    string.IsNullOrEmpty(userModel.Password))
                    return BadRequest();

                var userDb = await _userMananger
                    .GetByLoginAsyncNoTracking(userModel.Login);

                // user not found
                if (userDb == null)
                    return NotFound();

                // invalid password
                if (!userDb.Password.Equals(
                        AccountHelper.Encrypt(userModel.Password)))
                    return BadRequest();

                // inactive
                if (!userDb.Active)
                    return BadRequest();

                var token = AccountHelper
                    .GenerateToken(_configuration, userDb);

                var userResponse = _mapper
                    .Map<UserLoginSuccessViewModel>(userDb);

                // remove passwork
                userResponse.Password = null;
                // add access token
                userResponse.AccessToken = token;

                var response = new ResponseSuccessViewModel
                {
                    Results = userResponse,
                    StatusMessage = MessageHelper.SuccessResponse
                };                

                return Ok(response);
            }
            catch (Exception)
            {                
                return BadRequest();
            }
        }

        /// <summary>
        /// Update a user
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>

        [HttpPut, Route(""), Authorize]
        [Produces("application/json")]
        public async Task<IActionResult> Update(
            [FromBody] UserViewModel userViewModel
        )
        {
            try
            {
                var userModel = _mapper.Map<UserModel>(userViewModel);

                var userDb = await _userMananger
                    .GetByIdAsync(userViewModel.Id);

                if (userDb == null)
                    return BadRequest();

                if (!string.IsNullOrEmpty(userViewModel.FullName))
                    userDb.FullName = userModel.FullName;

                if (!string.IsNullOrEmpty(userViewModel.Password))
                    userDb.Password = AccountHelper
                        .Encrypt(userViewModel.Password);
                
                userDb.UpdatedAt = DataHelper.CurrentDateTime();

                _userMananger.Update(userDb);

                _uow.Commit();

                var userResponse = _mapper.Map<UserViewModel>(userDb);

                var response = new ResponseSuccessViewModel
                {
                    Results = new List<dynamic>
                    {
                        userResponse
                    },
                    StatusMessage = MessageHelper.SuccessResponse
                }; 

                return Ok(response);
            }
            catch (Exception)
            {               
                return BadRequest();
            }
        }

        /// <summary>
        /// Drop user
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        [HttpDelete, Route("{id}"), Authorize]
        [Produces("application/json")]
        public async Task<IActionResult> Remove(
            [FromRoute] long id
        )
        {
            try
            {
                var userDb = await _userMananger
                    .GetByIdAsync(id);

                if (userDb == null)
                    return BadRequest();

                userDb.Active = false;

                userDb.UpdatedAt = DataHelper.CurrentDateTime();

                _userMananger.Update(userDb);

                _uow.Commit();

                var response = new ResponseSuccessViewModel
                {
                    Results = null,
                    StatusMessage = MessageHelper.SuccessResponse
                };

                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Get All user actives
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>

        [HttpGet, Route(""), Authorize(Policy = "administrator")]
        [Produces("application/json")]
        public async Task<IActionResult> GetAll(
            [FromQuery] int page = 1
        )
        {
            try
            {
                if (page < 1)
                    page = 1;
                
                var offset = (page == 1? 0: (page * 30));

                var usersActive = await _userMananger
                    .GetAllActives(offset);

                var response = new ResponseSuccessViewModel
                {
                    Results = usersActive,
                    StatusMessage = MessageHelper.SuccessResponse
                };

                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}