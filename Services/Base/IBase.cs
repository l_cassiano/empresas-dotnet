namespace empresas_dotnet.Services
{
    public interface IBase<T> where T : class
    {
         void Create(T model);
         void Update(T model);
         void Remove(long Id);
    }
}