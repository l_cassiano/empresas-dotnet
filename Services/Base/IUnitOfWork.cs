namespace empresas_dotnet.Services
{
    public interface IUnitOfWork
    {
         void Commit();
         void RollBack();
    }
}