using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using empresas_dotnet.Models;

namespace empresas_dotnet.Services
{
    public interface IMovie : IBase<MovieModel>
    {
        Task<MovieModel> GetByTitleAsyncNoTracking(string title);
        Task<MovieModel> GetById(long id);
        Task<List<MovieModel>> GetAll(string director, string title, string genre, string[] actors, int offset);
    }
}