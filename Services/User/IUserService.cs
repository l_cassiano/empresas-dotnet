using System.Collections.Generic;
using System.Threading.Tasks;
using empresas_dotnet.Models;

namespace empresas_dotnet.Services
{
    public interface IUserService : IBase<UserModel>
    {
        Task<UserModel> GetByLoginAsyncNoTracking(string login);
        Task<UserModel> GetByIdAsync(long id);
        Task<List<UserModel>> GetAllActives(int offset);

    }
}