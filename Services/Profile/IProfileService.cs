
using System.Threading.Tasks;
using empresas_dotnet.Models;

namespace empresas_dotnet.Services
{
    public interface IProfileService
    {
        Task<ProfileModel> GetByProfileName(string name);
    }
}